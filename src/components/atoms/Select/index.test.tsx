import { screen, render } from '@testing-library/react';
import React from 'react';

import Select from './index';

describe('<Select />', () => {
  test('should have exact value props', () => {
    const options = [
      {
        value: 'desc',
        label: 'Desc',
      },
      {
        value: 'asc',
        label: 'Asc',
      },
    ];
    const name = 'test';

    const { container } = render(
      <Select name={name} label="test-label" options={options} />
    );
    expect(container.getElementsByTagName('label')[0].textContent).toEqual(
      'test-label'
    );
  });
});
