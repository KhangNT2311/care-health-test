import {
  FormControl,
  InputLabel,
  MenuItem,
  SelectProps,
  Select as MuiSelect,
} from '@mui/material';
import React from 'react';
interface IOption {
  label: string;
  value: string;
}
interface ISelect extends SelectProps {
  label: string;
  options: IOption[];
}
const Select: React.FC<ISelect> = ({ label, options, ...props }) => {
  return (
    <FormControl fullWidth>
      <InputLabel>{label}</InputLabel>
      <MuiSelect label={label} {...props}>
        {options.map(({ value, label }) => (
          <MenuItem value={value} key={value}>
            {label}
          </MenuItem>
        ))}
        {/* <MenuItem value={10}>Ten</MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty</MenuItem> */}
      </MuiSelect>
    </FormControl>
  );
};

export default Select;
