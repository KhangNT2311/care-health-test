import {
  Box,
  ButtonBase,
  Grid,
  Input,
  InputBase,
  Typography,
} from '@mui/material';
import Image from 'next/image';
import React, { ChangeEvent } from 'react';
import iconSearch from '../../../assets/image/icon-search.png';
import style from '../../../styles/Home.module.css';

type Props = {};
interface ISearchBox {
  handleSearch: (e: ChangeEvent<HTMLInputElement>) => void;
}

const SearchBox: React.FC<ISearchBox> = ({ handleSearch }) => {
  return (
    <Grid sx={{ marginBottom: '20px' }}>
      <Typography component={'p'} sx={{ fontSize: '25px', fontWeight: 'bold' }}>
        Search
      </Typography>
      <Grid
        className={style.search__box}
        sx={{
          position: 'relative',
          border: '1px solid #c3c3c3',
          padding: '5px 20px',
          borderRadius: '20px',
          height: '100%',
          transition: '0.3s ease-in-out',
        }}
      >
        <InputBase
          sx={{ width: '100%', paddingRight: '30px' }}
          onChange={handleSearch}
          inputProps={{
            'data-testid': 'input',
          }}
        />
        <ButtonBase
          type="submit"
          sx={{ position: 'absolute', right: '10px', top: '5px' }}
        >
          <Image src={iconSearch} />
        </ButtonBase>
      </Grid>
    </Grid>
  );
};

export default SearchBox;
