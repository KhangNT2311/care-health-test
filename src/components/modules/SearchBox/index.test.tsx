import { screen, render, fireEvent, waitFor } from '@testing-library/react';
import React from 'react';

import SearchBox from './index';

describe('<SearchBox />', () => {
  test('should call handleSearch when change input', async () => {
    const handleSearch = jest.fn();

    render(<SearchBox handleSearch={handleSearch} />);
    const input = screen.getByTestId('input');
    await fireEvent.change(input, {
      target: { value: 'Hello' },
    });
    await waitFor(
      () => {
        expect(handleSearch).toBeCalled();
      },
      { timeout: 1000 }
    );
  });
});
