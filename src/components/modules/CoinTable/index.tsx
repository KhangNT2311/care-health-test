import {
  Box,
  Pagination,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import Link from 'next/link';
import React from 'react';
import style from '../../../styles/Home.module.css';

interface CoinListDetail {
  coinList: ICoinData[];
}

const coinDetail: CoinDetail[] = [
  {
    name: 'Coin name',
    align: 'left',
  },
  {
    name: 'Coin symbol',
    align: 'center',
  },
  {
    name: 'Coin logo',
    align: 'right',
  },
  {
    name: 'Current price',
    align: 'right',
  },
  {
    name: 'Current total market cap',
    align: 'right',
  },
  {
    name: 'The price changes',
    align: 'right',
  },
];

const CoinTable: React.FC<CoinListDetail> = ({ coinList }) => {
  return (
    <TableContainer component={Paper} sx={{ width: '100%' }}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {coinDetail.map((coin, index) => {
              return (
                <TableCell
                  key={index}
                  align={coin.align}
                  sx={{ fontWeight: 'bold' }}
                >
                  {coin.name}
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {coinList?.length > 0 ? (
            coinList.map((coin) => (
              <TableRow
                className={style.coin__row}
                key={coin.name}
                sx={{ transition: '0.3s ease-in-out' }}
              >
                <TableCell component="th" scope="row">
                  <Link passHref href={`coin/${coin.uuid}`}>
                    <a className={style.coin__link}>{coin.name}</a>
                  </Link>
                </TableCell>
                <TableCell align="center">{coin.symbol}</TableCell>
                <TableCell align="center">
                  <Box
                    component={'img'}
                    src={coin.iconUrl}
                    alt="coin logo"
                    sx={{ width: '20px' }}
                  />
                </TableCell>
                <TableCell align="right">
                  {Intl.NumberFormat().format(coin.price)}
                </TableCell>
                <TableCell align="right">
                  {Intl.NumberFormat().format(coin.marketCap)}
                </TableCell>
                <TableCell align="right">
                  {Intl.NumberFormat().format(coin.change)}
                </TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={coinDetail.length} align="center">
                No content
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CoinTable;
