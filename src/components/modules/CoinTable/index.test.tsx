import { screen, render } from '@testing-library/react';
import React from 'react';

import CoinTable from './index';

describe('<CoinTable />', () => {
  test('should have exact rows length', () => {
    const data = [
      {
        uuid: 'Qwsogvtv82FCd',
        symbol: 'BTC',
        name: 'Bitcoin',
        color: '#f7931A',
        iconUrl: 'https://cdn.coinranking.com/bOabBYkcX/bitcoin_btc.svg',
        marketCap: 369025980336,
        price: 19264.43341317478,
        listedAt: 1330214400,
        tier: 1,
        change: 3.43,
        rank: 1,

        lowVolume: false,
        coinrankingUrl:
          'https://coinranking.com/coin/Qwsogvtv82FCd+bitcoin-btc',
        '24hVolume': '34668391414',
        btcPrice: '1',
      },
      {
        uuid: 'razxDUgYGNAdQ',
        symbol: 'ETH',
        name: 'Ethereum',
        color: '#3C3C3D',
        iconUrl: 'https://cdn.coinranking.com/rk4RKHOuW/eth.svg',
        marketCap: 165426079791,
        price: 1355.0396101586086,
        listedAt: 1438905600,
        tier: 1,
        change: 3.31,
        rank: 2,

        lowVolume: false,
        coinrankingUrl:
          'https://coinranking.com/coin/razxDUgYGNAdQ+ethereum-eth',
        '24hVolume': '14496471114',
        btcPrice: '0.070338928796728',
      },
      {
        uuid: 'HIVsRcGKkPFtW',
        symbol: 'USDT',
        name: 'Tether USD',
        color: '#22a079',
        iconUrl: 'https://cdn.coinranking.com/mgHqwlCLj/usdt.svg',
        marketCap: 67908714875,
        price: 0.9997798506391594,
        listedAt: 1420761600,
        tier: 1,
        change: 0.36,
        rank: 3,

        lowVolume: false,
        coinrankingUrl:
          'https://coinranking.com/coin/HIVsRcGKkPFtW+tetherusd-usdt',
        '24hVolume': '46043853312',
        btcPrice: '0.000051897703358118',
      },
    ];
    const { container } = render(<CoinTable coinList={data} />);
    expect(container.querySelectorAll('tbody tr').length).toEqual(3);
  });
});
