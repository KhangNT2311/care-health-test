import { Box, ButtonBase, Grid } from '@mui/material';
import Image from 'next/image';
import React from 'react';
import logo from '../../../assets/image/logo.png';
import menu from '../../../assets/image/menu-icon.png';
import style from '../../../styles/Header.module.css';

const Header = () => {
  return (
    <Grid
      sx={{
        position: 'fixed',
        top: '0',
        left: '0',
        width: '100vw',
        height: '60px',
        backgroundColor: '#60a9ff',
        padding: '0 15px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        zIndex: '100000',
      }}
    >
      <Image
        className={style.logo}
        src={logo}
        data-testid="logo"
        alt="logo"
        width={'40px'}
        height={'40px'}
      />
      <ButtonBase sx={{ display: { xs: 'block', md: 'none' } }}>
        <Image
          className={style.logo}
          src={menu}
          alt="menu icon"
          width={'40px'}
          height={'40px'}
        />
      </ButtonBase>
    </Grid>
  );
};

export default Header;
