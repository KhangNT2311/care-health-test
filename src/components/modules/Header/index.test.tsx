import { screen, render } from '@testing-library/react';
import React from 'react';

import Header from './index';

describe('<Header />', () => {
  test('should render logo', () => {
    const { getByTestId } = render(<Header />);
    expect(getByTestId('logo').getAttribute('alt')).toEqual('logo');
  });
});
