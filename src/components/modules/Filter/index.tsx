import { ButtonBase, Grid, Typography } from '@mui/material';
import React from 'react';
import Select from '../../atoms/Select';
import useFilter from './hooks/useFilter';

interface IFilter {
  handleSort: (orderBy: string, orderDirection: string) => () => void;
}

const sortByOptions = [
  {
    value: 'change',
    label: 'Price Change',
  },
  {
    value: 'marketCap',
    label: 'Market Cap',
  },
  {
    value: 'price',
    label: 'Price',
  },
];

const sortDirections = [
  {
    value: 'desc',
    label: 'Desc',
  },
  {
    value: 'asc',
    label: 'Asc',
  },
];
const Filter: React.FC<IFilter> = ({ handleSort }) => {
  const { params, handleChange } = useFilter();
  return (
    <Grid sx={{ marginBottom: '20px' }}>
      <Typography component={'p'} sx={{ fontSize: '25px', fontWeight: 'bold' }}>
        Filter
      </Typography>
      <Grid
        sx={{
          display: 'flex',
          alignItems: { xs: 'flex-start', md: 'center' },
          justifyContent: 'space-between',
          flexDirection: { xs: 'column', md: 'row' },
        }}
      >
        <Grid
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: { xs: 'column', md: 'row' },
            padding: '20px 0 0 0 ',
          }}
        >
          <Select
            data-testid="btn-sortBy"
            sx={{
              marginBottom: '20px',
              width: { xs: '150px', md: '150px' },
              marginRight: { xs: '0', md: '20px' },
            }}
            name="orderBy"
            label="Sort By"
            options={sortByOptions}
            onChange={handleChange}
          />
          <Select
            data-testid="btn-sortDirection"
            sx={{ marginBottom: '20px', width: { xs: '150px', md: '150px' } }}
            name="orderDirection"
            label="Sort Direction"
            options={sortDirections}
            onChange={handleChange}
          />
        </Grid>
        <ButtonBase
          sx={{
            padding: { xs: '10px 50px', md: '10px 30px' },
            backgroundColor: '#60a9ff',
            borderRadius: '8px',
            color: '#fff',
            fontSize: '20px',
          }}
          onClick={handleSort(params.orderBy, params.orderDirection)}
        >
          Sort
        </ButtonBase>
      </Grid>
    </Grid>
  );
};

export default Filter;
