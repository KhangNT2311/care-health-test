import { SelectChangeEvent } from '@mui/material';
import React, { ChangeEvent, useState } from 'react';

function useFilter() {
  const [params, setParams] = useState({
    orderBy: '',
    orderDirection: '',
  });
  const handleChange = (e: SelectChangeEvent<unknown>) => {
    const {
      target: { name, value },
    } = e;
    setParams({
      ...params,
      [name]: value,
    });
  };
  return {
    params,
    handleChange,
  };
}

export default useFilter;
