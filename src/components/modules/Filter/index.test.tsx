import { screen, render, waitFor } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';

import Filter from './index';

describe('<Filter />', () => {
  test('should call handleSort when click sort', async () => {
    const handleSort = jest.fn();

    render(<Filter handleSort={handleSort} />);
    await userEvent.click(screen.getByText('Sort'));
    await waitFor(() => expect(handleSort).toBeCalled());
  });
});
