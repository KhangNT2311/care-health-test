import {
  TableCell,
  Grid,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableBody,
  Paper,
  Box,
  Pagination,
} from '@mui/material';
import React, { useMemo } from 'react';
import { IStats } from '../../../types/statType.d';
import CoinTable from 'components/modules/CoinTable';
import Filter from 'components/modules/Filter';
import SearchBox from 'components/modules/SearchBox';
import useActions from './hooks/useActions';
import { CoinListDto } from 'dto/CoinDTO';
import { StatsDto } from 'dto/StatsDTO';
interface IHome {
  coins: ICoinData[];
  stats: IStats;
}

const Home: React.FC<IHome> = ({ coins: coinsProp, stats: statsProp }) => {
  const defaultCoins = useMemo(() => CoinListDto(coinsProp), [coinsProp]);
  const defaultStats = useMemo(() => new StatsDto(statsProp), [statsProp]);
  const defaultParams = useMemo(
    () => ({
      offset: 0,
      limit: 10,
      total: defaultStats?.total,
      search: '',
      orderBy: '',
      orderDirection: '',
    }),
    [defaultStats]
  );
  const {
    handleSearch,
    handleChangePagination,
    handleSort,
    params,
    coins,
    total,
  } = useActions({
    defaultCoins,
    defaultParams,
    defaultStats,
  });
  return (
    <Grid sx={{ maxWidth: '1200px', margin: 'auto', padding: '20px 10px' }}>
      <SearchBox handleSearch={handleSearch} />
      <Filter handleSort={handleSort} />
      <CoinTable coinList={coins} />
      <Grid
        sx={{
          width: '100%',
          marginTop: '20px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Pagination
          count={Math.ceil(total / params.limit)}
          color="primary"
          onChange={handleChangePagination}
          page={(params.offset || 0) + 1}
        />
      </Grid>
    </Grid>
  );
};

export default Home;
