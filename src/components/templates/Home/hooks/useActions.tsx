import React, {
  ChangeEvent,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import useCoins from '../../../../hooks/useCoins';
import { IStats } from '../../../../types/statType.d';

const TIME_OUT = 500;

interface IUseActions {
  defaultCoins: ICoinData[];
  defaultParams: ICoinParams | null;
  defaultStats: IStats | null;
}

const useActions = ({
  defaultCoins = [],
  defaultParams = null,
  defaultStats = null,
}: IUseActions) => {
  const [params, setParams] = useState(
    defaultParams || {
      search: '',
      limit: 0,
      offset: 0,
      orderBy: '',
      orderDirection: '',
    }
  );

  const isFirstRender = useRef<boolean>(true);
  const { fetchCoins, coins, stats } = useCoins({ defaultCoins, defaultStats });

  useEffect(() => {

    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else {
      fetchCoins(params);
    }
    // eslint-disable-next-line
  }, [params]);

  const timeoutRef = useRef<ReturnType<typeof setTimeout> | null>(null);
  const handleSearch = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (timeoutRef.current) clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      setParams({
        ...params,
        search: e.target.value,
        offset: 0,
      });
    }, TIME_OUT);
  };

  const handleChangePagination = (e: ChangeEvent<unknown>, page: number) => {
    setParams({
      ...params,
      offset: page - 1,
    });
  };

  const handleSort = (orderBy: string, orderDirection: string) => () => {
    setParams({
      ...params,
      orderBy,
      orderDirection,
      offset: 0,
    });
  };
  const total = useMemo(() => stats.total, [stats]);
  return {
    handleSearch,
    handleChangePagination,
    handleSort,
    params,
    coins,
    total,
  };
};

export default useActions;
