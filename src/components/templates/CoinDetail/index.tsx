import { Box, ButtonBase, Grid, Typography } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import CountUp from "react-countup";
import backIcon from "../../../assets/image/back-icon.png";
import coinBg from "../../../assets/image/coin-bg.jpg";
import style from "../../../styles/Detail.module.css";

interface ICoinDetail {
  id: string;
  detail: {
    name: string;
    symbol: string;
    iconUrl: string;
    price: number;
    marketCap: number;
  };
}

const CoinDetail: React.FC<ICoinDetail> = ({ id, detail }) => {
  const coinPrice = Intl.NumberFormat().format(detail.price);
  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: "#111b4e",
        height: "100vh",
      }}
    >
      <Grid
        sx={{
          padding: "50px 10px 20px 10px",
          position: "relative",
          maxWidth: "1200px",
          margin: "auto",
        }}
      >
        <Link href="/" passHref>
          <a className={style.back__to__home}>
            <Image
              src={backIcon}
              alt="back icon"
              width={"40px"}
              height={"40px"}
            />
          </a>
        </Link>
        <Grid
          className={style.coin__wrapper}
          sx={{
            display: "flex",
            alignItems: "flex-start",
            justifyContent: "space-between",
            flexDirection: "column",
            padding: "15px 10px",
            width: "100%",
            height: "200px",
            borderRadius: "30px",
          }}
        >
          <Grid
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Box
              component={"img"}
              src={detail.iconUrl}
              alt="icon url"
              sx={{ width: "32px", height: "32px", marginRight: "10px" }}
            />
            <Typography
              component={"p"}
              sx={{ fontSize: "25px", fontWeight: "bold", color: "#fff" }}
            >
              {detail.name}
            </Typography>
          </Grid>
          <Typography
            component={"p"}
            sx={{ color: "#e9caa5", fontSize: "25px" }}
          >
            ${coinPrice}
          </Typography>
          <Box
            sx={{
              fontSize: "25px",
              fontWeight: "bold",
              color: "#e9caa5",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
            }}
          >
            {detail.symbol}
            <Typography
              component={"p"}
              sx={{ fontSize: "10px", color: "#fff" }}
            >
              Id: {id}
            </Typography>
          </Box>
        </Grid>
        <Grid
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            marginTop: "20px",
          }}
        >
          <Typography
            component={"p"}
            sx={{ color: "#e9caa5", fontSize: "25px", textAlign: "right" }}
          >
            MarketCap: ${Intl.NumberFormat().format(detail.marketCap)}
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default CoinDetail;
