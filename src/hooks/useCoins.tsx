import React, { useState } from 'react';
import CoinAPI from '../api/actions/coins';
import { CoinListDto } from '../dto/CoinDTO';
import { StatsDto } from '../dto/StatsDTO';
import { IStats } from '../types/statType.d';

interface IUseCoins {
  defaultCoins: ICoinData[];
  defaultStats: IStats | null;
}
function useCoins({ defaultCoins = [], defaultStats = null }: IUseCoins) {
  const [coins, setCoins] = useState(defaultCoins);
  const [stats, setStats] = useState(
    defaultStats || {
      total: 0,
    }
  );
  const fetchCoins = async (params: ICoinParams) => {
    try {
      const updatedParams = Object.fromEntries(
        Object.entries(params).filter(([_, v]) => !!v)
      );
      const response = await CoinAPI.getAll(updatedParams);
      setCoins(CoinListDto(response?.data?.coins));
      setStats(new StatsDto(response.data.stats));
    } catch (error) {}
  };

  return { fetchCoins, coins, stats };
}

export default useCoins;
