import { ICoinData } from '../types/coinType';

export class CoinDto {
  uuid: string;
  name: string;
  symbol: string;
  iconUrl: string;
  price: number;
  marketCap: number;
  change: number;
  constructor(data: ICoinData) {
    this.uuid = data.uuid;
    this.name = data.name;
    this.symbol = data.symbol;
    this.iconUrl = data.iconUrl;
    this.price = data.price;
    this.marketCap = data.marketCap;
    this.change = data.change;
  }
}

export const CoinListDto = (coins: ICoinData[]) => {
  return coins.map((coin) => new CoinDto(coin));
};
