import { IStats } from '../types/statType';

export class StatsDto {
  total: number;
  constructor(data: IStats) {
    this.total = data.total;
  }
}
