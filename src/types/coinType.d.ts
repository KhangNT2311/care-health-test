interface ICoinData {
  uuid: string;
  name: string;
  symbol: string;
  iconUrl: string;
  price: number;
  marketCap: number;
  change: number;
}
interface CoinDetail {
  name: string;
  align: 'left' | 'center' | 'right';
}

interface ICoinParams {
  search?: string;
  limit: number;
  offset?: number;
  orderBy?: string;
  orderDirection?: string;
}
