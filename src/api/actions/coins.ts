import API from 'api';
import config from 'api/config';

const getAll = async (params: ICoinParams) => {
  try {
    const coins = await API({
      method: 'GET',
      url: config.API.COIN_SERVICE,
      params,
    });
    return coins;
  } catch (error) {
    return error;
  }
};

const get10Items = async () => {
  try {
    const user = await API({
      method: 'GET',
      url: config.API.COIN_SERVICE,
      params: {
        limit: 10,
      },
    });
    return user;
  } catch (error) {
    return error;
  }
};

const getCoinDetail = async (uuids: string) => {
  try {
    const user = await API({
      method: 'GET',
      url: config.API.COIN_SERVICE,
      params: {
        uuids,
      },
    });
    return user;
  } catch (error) {
    return error;
  }
};

const CoinAPI = {
  getAll,
  get10Items,
  getCoinDetail,
};

export default CoinAPI;
