import axios from 'axios';

const mockToken = 'coinrankingac2627b744222b4a91d5b79cb01d41ffcf9c43e3c611da7b';

// Add a request interceptor
axios.interceptors.request.use(
  async (config: any) => {
    config.headers['x-access-token'] = mockToken;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalConfig = error.config;

    if (error.response) {
      try {
        return originalConfig;
      } catch (e) {
        return Promise.reject(error);
      }
    }
    return Promise.reject(error);
  }
);
