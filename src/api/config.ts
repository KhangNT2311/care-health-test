import { IAPIEndpoint, IConfig } from './api.d';

const NEXT_PUBLIC_API_URL =
  process.env.NEXT_PUBLIC_RESTFUL_ENDPOINT || 'https://api.coinranking.com/v2';
const config: IConfig = {
  API: {
    COIN_SERVICE: '/coins',
  },
};

Object.keys(config.API).forEach((item) => {
  config.API[item as keyof IAPIEndpoint] =
    NEXT_PUBLIC_API_URL + config.API[item as keyof IAPIEndpoint];
});

export default config;
