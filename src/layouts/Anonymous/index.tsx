import React from "react";
import Header from "components/modules/Header";
interface IAnonymousLayout {
  children: React.ReactNode;
}
const AnonymousLayout: React.FC<IAnonymousLayout> = ({ children }) => {
  return (
    <>
      <Header />
      <main style={{ paddingTop: 50 }}>{children}</main>
    </>
  );
};

export default AnonymousLayout;
