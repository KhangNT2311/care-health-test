import '../styles/globals.css';
import type { AppProps } from 'next/app';
import AnonymousLayout from '../layouts/Anonymous';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AnonymousLayout>
      <Component {...pageProps} />
    </AnonymousLayout>
  );
}

export default MyApp;
